<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
    public function gets(){
    	return DB::table('categories')->orderBy('created_at', 'desc')->get();
    }
}
