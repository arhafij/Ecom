<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{

    public function __construct()
    {
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category_model = new Category();
        $categories = $category_model->gets();
        $options = [];
        foreach ($categories as $item) {
            $options[$item->id] = $item->name;
        }
        array_unshift($options, "Select...");
        return view('admin.category.create',compact('options', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
            'name'=>'required|unique:categories'
        ]);
        $category = new Category();
        $category->name= $request->name;
        $category->parent_id = ( $category->parent_id == 0 ) ? null : $request->parent_id;
        if($category->save()) {
            return redirect()->back()->with('success','Succesfully created.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        $options = [];
        foreach ($categories as $item) {
            $options[$item->id] = $item->name;
        }
        array_unshift($options, "Select..");
        return view('admin.category.edit', compact('category','options'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|unique:categories,id'
        ]);
        $category = Category::find($id);
        $category->name= $request->name;
        $category->parent_id = ( $category->parent_id == 0 ) ? null : $request->parent_id;
        if($category->save()) {
            return redirect()->back()->with('success','Succesfully updated.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return back();
    }
}
