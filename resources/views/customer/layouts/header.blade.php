<!--Top-->
<nav id="top">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <select class="language">
                    <option value="English" selected>English</option>
                    <option value="France">France</option>
                    <option value="Germany">Germany</option>
                </select>
                <select class="currency">
                    <option value="USD" selected>USD</option>
                    <option value="EUR">EUR</option>
                    <option value="DDD">DDD</option>
                </select>
            </div>
            <div class="col-xs-6">
                <ul class="top-link">
                    <li><a href="account.html"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                    <li><a href="contact.html"><span class="glyphicon glyphicon-envelope"></span> Contact</a></li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<!--Header-->
<header >
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div id="logo"><img src="{{ asset('customer/images/logo.png') }}" /></div>
            </div>
            <div class="col-md-6 text-right">
                <div class="phone"><span class="glyphicon glyphicon-earphone"></span>0123-456-789</div>
                <div class="mail"><span class="glyphicon glyphicon-envelope"></span>infor@yoursite.com</div>
                <form class="form-search">
                    <input type="text" class="input-medium search-query">
                    <button type="submit" class="btn"><span class="glyphicon glyphicon-search"></span></button>
                </form>
            </div>
            <div id="cart"><a class="btn btn-cart" href="cart.html"><span class="glyphicon glyphicon-shopping-cart"></span>CART<strong>0</strong></a></div>
        </div>
    </div>
</header>
<!--Navigation-->
<nav id="menu" class="navbar">
    <div class="container">
        <div class="navbar-header"><span id="heading" class="visible-xs">Categories</span>
          <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"><i class="fa fa-bars"></i></button>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="index.html">Home</a></li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Women Fashion</a>
                    <div class="dropdown-menu">
                        <div class="dropdown-inner">
                            <ul class="list-unstyled">
                                <li><a href="category.html">Text 101</a></li>
                                <li><a href="category.html">Text 102</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Men Fashion</a>
                    <div class="dropdown-menu">
                        <div class="dropdown-inner">
                            <ul class="list-unstyled">
                                <li><a href="category.html">Text 201</a></li>
                                <li><a href="category.html">Text 202</a></li>
                                <li><a href="category.html">Text 203</a></li>
                                <li><a href="category.html">Text 204</a></li>
                                <li><a href="category.html">Text 205</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Kids Fashion</a>
                    <div class="dropdown-menu" style="margin-left: -203.625px;">
                        <div class="dropdown-inner">
                            <ul class="list-unstyled">
                                <li><a href="category.html">Text 301</a></li>
                                <li><a href="category.html">Text 302</a></li>
                                <li><a href="category.html">Text 303</a></li>
                                <li><a href="category.html">Text 304</a></li>
                                <li><a href="category.html">Text 305</a></li>
                            </ul>
                            <ul class="list-unstyled">
                                <li><a href="category.html">Text 306</a></li>
                                <li><a href="category.html">Text 307</a></li>
                                <li><a href="category.html">Text 308</a></li>
                                <li><a href="category.html">Text 309</a></li>
                                <li><a href="category.html">Text 310</a></li>
                            </ul>
                            <ul class="list-unstyled">
                                <li><a href="category.html">Text 311</a></li>
                                <li><a href="category.html">Text 312</a></li>
                                <li><a href="category.html#">Text 313</a></li>
                                <li><a href="category.html#">Text 314</a></li>
                                <li><a href="category.html">Text 315</a></li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li><a href="category.html">New Fashion</a></li>
                <li><a href="category.html">Hot Fashion</a></li>
            </ul>
        </div>
    </div>
</nav>
