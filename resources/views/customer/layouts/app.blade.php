<!DOCTYPE html>
<html lang="en">

  <head>
    @include('customer.layouts.head')

    @section('css')
    @show
  </head>

  <body>
    @include('customer.layouts.header')

    @section('main-content')
    @show

    @include('customer.layouts.footer')

    @section('js')
    @show
  </body>

</html>
