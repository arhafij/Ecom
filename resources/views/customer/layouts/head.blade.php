<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="description" content="Free Bootstrap Themes by 365Bootstrap dot com - Free Responsive Html5 Templates">
<meta name="author" content="https://www.365bootstrap.com">

<title>Ecom</title>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{ asset('customer/css/bootstrap.min.css') }}"  type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="{{ asset('customer/css/style.css') }}">


<!-- Custom Fonts -->
<link rel="stylesheet" href="{{ asset('customer/font-awesome/css/font-awesome.min.css') }}"  type="text/css">
<link rel="stylesheet" href="{{ asset('customer/fonts/font-slider.css" type="text/css') }}">

<!-- jQuery and Modernizr-->
<script src="{{ asset('customer/js/jquery-2.1.1.js') }}"></script>

<!-- Core JavaScript Files -->
<script src="{{ asset('customer/js/bootstrap.min.js') }}"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
<![endif]-->
