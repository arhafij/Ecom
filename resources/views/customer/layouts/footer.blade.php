<footer>
    <div class="brand">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <a href="#"><img src="{{ asset('customer/images/brand1-250x100.jpg') }}" /></a>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <a href="#"><img src="{{ asset('customer/images/brand2-250x100.jpg') }}" /></a>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <a href="#"><img src="{{ asset('customer/images/brand1-250x100.jpg') }}" /></a>
                </div>
                <div class="col-lg-3 col-xs-6">
                    <a href="#"><img src="{{ asset('customer/images/brand4-250x100.jpg') }}" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-right">
                    <h4>Subcribe Email</h4>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="col-md-6">
                    <form name="subcribe-email" action="subcribe.php">
                        <div class="subcribe-form form-group">
                            <input class="form-inline" type="text" name="email" value="1"><button href="#" class="btn btn-4" type="submit">Subcribe</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="wrap-footer">
            <div class="row">
                <div class="col-md-3 col-footer footer-1">
                    <img src="{{ asset('customer/images/logofooter.png') }}" />
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
                <div class="col-md-3 col-footer footer-2">
                    <div class="heading"><h4>Customer Services</h4></div>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Delivery Information</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Terms & Conditions</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-footer footer-3">
                    <div class="heading"><h4>My Account</h4></div>
                    <ul>
                        <li><a href="#">My Account</a></li>
                        <li><a href="#">Brands</a></li>
                        <li><a href="#">Gift Vouchers</a></li>
                        <li><a href="#">Specials</a></li>
                        <li><a href="#">Site Map</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-footer footer-4">
                    <div class="heading"><h4>Contact Us</h4></div>
                    <ul>
                        <li><span class="glyphicon glyphicon-home"></span>California, United States 3000009</li>
                        <li><span class="glyphicon glyphicon-earphone"></span>+91 8866888111</li>
                        <li><span class="glyphicon glyphicon-envelope"></span>infor@yoursite.com</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    Your Store ?? 20xx - Designed by <a href="https://www.365bootstrap.com" target="_blank">365bootstrap</a>
                </div>
                <div class="col-md-6">
                    <div class="pull-right">
                        <ul>
                            <li><img src="{{ asset('customer/images/visa-curved-32px.png') }}" /></li>
                            <li><img src="{{ asset('customer/images/paypal-curved-32px.png') }}" /></li>
                            <li><img src="{{ asset('customer/images/discover-curved-32px.png') }}" /></li>
                            <li><img src="{{ asset('customer/images/maestro-curved-32px.png') }}" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- JS -->
<script>
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a').on('shown.bs.tab', function(event){
        var x = $(event.target).text();         // active tab
        var y = $(event.relatedTarget).text();  // previous tab
        $(".act span").text(x);
        $(".prev span").text(y);
    });
});
</script>
