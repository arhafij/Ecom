@extends('admin.layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('admin/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('main-content')
<section class="content-header" style="margin-bottom: 20px;">
	<h1 style="padding-left: 20px;">
		Dashboard				
		{{-- <small>Management</small> --}}
	</h1>
	<ol class="breadcrumb" style="padding-right: 20px;">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		{{-- <li class="active">Products</li> --}}
	</ol>
</section>

@endsection

@section('js')
<script src="{{ asset('admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
	$('.select2').select2();
</script>
@endsection