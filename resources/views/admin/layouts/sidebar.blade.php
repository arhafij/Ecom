    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar-wrapper">
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <div class="page-sidebar md-shadow-z-2-i  navbar-collapse collapse">
            <!-- BEGIN SIDEBAR MENU -->
            <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
            <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
            <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
            <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
            <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
            <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
            <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                <li class="start">
                    <a href="index.html">
                    <i class="icon-home"></i>
                    <span class="title">Dashboard</span>
                    </a>
                </li>
                <li 
                class="{{ in_array( Route::currentRouteName(), ['categories.create','categories.edit'] ) ? 'active': '' }}">
                    <a href="{{ route('categories.create') }}">
                    <i class=" icon-grid"></i>
                    <span class="title">Categories</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;">
                    <i class="icon-basket"></i>
                    <span class="title">eCommerce</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="ecommerce_index.html">
                            <i class="icon-home"></i>
                            Dashboard</a>
                        </li>
                        <li>
                            <a href="ecommerce_orders.html">
                            <i class="icon-basket"></i>
                            Orders</a>
                        </li>
                        <li>
                            <a href="ecommerce_orders_view.html">
                            <i class="icon-tag"></i>
                            Order View</a>
                        </li>
                        <li>
                            <a href="ecommerce_products.html">
                            <i class="icon-handbag"></i>
                            Products</a>
                        </li>
                        <li>
                            <a href="ecommerce_products_edit.html">
                            <i class="icon-pencil"></i>
                            Product Edit</a>
                        </li>
                    </ul>
                </li>
                <!-- BEGIN ANGULARJS LINK -->
                <li class="tooltips" data-container="body" data-placement="right" data-html="true" data-original-title="AngularJS version demo">
                    <a href="angularjs" target="_blank">
                    <i class="icon-paper-plane"></i>
                    <span class="title">
                    AngularJS Version </span>
                    </a>
                </li>
                <!-- END ANGULARJS LINK -->
                <li>
                    <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Form Stuff</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="form_controls_md.html">
                            <span class="badge badge-roundless badge-danger">new</span>Material Design<br>
                            Form Controls</a>
                        </li>
                        <li>
                            <a href="form_controls.html">
                            Bootstrap<br>
                            Form Controls</a>
                        </li>
                        <li>
                            <a href="form_layouts.html">
                            Form Layouts</a>
                        </li>
                        <li>
                            <a href="form_editable.html">
                            <span class="badge badge-warning">new</span>Form X-editable</a>
                        </li>
                        <li>
                            <a href="form_wizard.html">
                            Form Wizard</a>
                        </li>
                        <li>
                            <a href="form_validation.html">
                            Form Validation</a>
                        </li>
                        <li>
                            <a href="form_image_crop.html">
                            <span class="badge badge-danger">new</span>Image Cropping</a>
                        </li>
                        <li>
                            <a href="form_fileupload.html">
                            Multiple File Upload</a>
                        </li>
                        <li>
                            <a href="form_dropzone.html">
                            Dropzone File Upload</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:;">
                    <i class="icon-briefcase"></i>
                    <span class="title">Data Tables</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="table_basic.html">
                            Basic Datatables</a>
                        </li>
                        <li>
                            <a href="table_tree.html">
                            Tree Datatables</a>
                        </li>
                        <li>
                            <a href="table_responsive.html">
                            Responsive Datatables</a>
                        </li>
                        <li>
                            <a href="table_managed.html">
                            Managed Datatables</a>
                        </li>
                        <li>
                            <a href="table_editable.html">
                            Editable Datatables</a>
                        </li>
                        <li>
                            <a href="table_advanced.html">
                            Advanced Datatables</a>
                        </li>
                        <li>
                            <a href="table_ajax.html">
                            Ajax Datatables</a>
                        </li>
                    </ul>
                </li>
                <li class="last ">
                    <a href="javascript:;">
                    <i class="icon-pointer"></i>
                    <span class="title">Maps</span>
                    <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="maps_google.html">
                            Google Maps</a>
                        </li>
                        <li>
                            <a href="maps_vector.html">
                            Vector Maps</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- END SIDEBAR MENU -->
        </div>
    </div>
    <!-- END SIDEBAR -->