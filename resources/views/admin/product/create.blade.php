@extends('admin.layouts.app')
@section('css')
<link rel="stylesheet" href="{{ asset('admin/bower_components/select2/dist/css/select2.min.css') }}">
@endsection
@section('main-content')
<section class="content-header" style="margin-bottom: 20px;">
	<h1 style="padding-left: 20px;">
		Product				
		<small>Management</small>
	</h1>
	<ol class="breadcrumb" style="padding-right: 20px;">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Products</li>
	</ol>
</section>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-success">
				<div class="box-header with-border">
					<h3 class="box-title">Create Product</h3>
					<div class="box-tools pull-right">
						{{-- <span class="label label-primary">Label</span> --}}
					</div>
					<!-- /.box-tools -->
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					{{ Form::open(['route'=>'products.store']) }}
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('Name') }}
								{{ Form::text('name',null,['class'=>'form-control','placeholder'=>'Enter Product Name']) }}
							</div>
							<div class="form-group">
								{{ Form::label('Description') }}
								{{ Form::textarea('description',null,['class'=>'form-control','rows'=>3]) }}
							</div>
							<div class="form-group">
								{{ Form::label('Price') }}
								{{ Form::number('price',null,['class'=>'form-control','placeholder'=>'Enter Product Price']) }}
							</div>
							{{ Form::submit('Save',['class'=>'btn btn-success'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::checkbox('published',null) }}
								{{ Form::label('Published') }}
							</div>
							<div class="form-group">
								{{ Form::label('Category') }}
							  	{{ Form::select('tag_id', ['Select...','food','kabab'], 0,
							  	['class'=>'form-control select2','data-placeholder'=>'Select a State'])}}
	                        </div>
							<div class="form-group">
								{{ Form::label('Tags') }}
							  	{{ Form::select('tag_id', ['food','kabab'], null,
							  	['class'=>'form-control select2','multiple'=>'multiple', 'data-placeholder'=>'Select a State'])}}
	                        </div>
	                        <div class="form-group">
								{{ Form::label('Upload Image') }}
								{{ Form::file('image',null,['class'=>'form-control']) }}
							</div>
						</div>
					{{ Form::close() }}
				</div>
				<!-- /.box-body -->
				<div class="box-footer">
				</div>
				<!-- box-footer -->
			</div>
			<!-- /.box -->
		</div>
	</div>
</div>
@endsection

@section('js')
<script src="{{ asset('admin/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
	$('.select2').select2();
</script>
@endsection