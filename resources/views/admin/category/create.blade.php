@extends('admin.layouts.app')
@section('css')
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/global/plugins/select2/select2.css') }}"> --}}
<link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/global/plugins/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/global/plugins/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('admin/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}">

@endsection
@section('main-content')
	<!-- BEGIN PAGE HEAD -->
	<div class="page-head">
		<!-- BEGIN PAGE TITLE -->
		<div class="page-title">
			<h1>Category <small>management</small></h1>
		</div>
		<!-- END PAGE TITLE -->
		<!-- BEGIN PAGE TOOLBAR -->
		<div class="page-toolbar">
			<!-- BEGIN THEME PANEL -->
			<div class="btn-group btn-theme-panel">
				<a href="javascript:;" class="btn dropdown-toggle" data-toggle="dropdown">
				<i class="icon-settings"></i>
				</a>
				<div class="dropdown-menu theme-panel pull-right dropdown-custom hold-on-click">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-12">
							<h3>THEME</h3>
							<ul class="theme-colors">
								<li class="theme-color theme-color-default" data-theme="default">
									<span class="theme-color-view"></span>
									<span class="theme-color-name">Dark Header</span>
								</li>
								<li class="theme-color theme-color-light active" data-theme="light">
									<span class="theme-color-view"></span>
									<span class="theme-color-name">Light Header</span>
								</li>
							</ul>
						</div>
						<div class="col-md-8 col-sm-8 col-xs-12 seperator">
							<h3>LAYOUT</h3>
							<ul class="theme-settings">
								<li>
									 Theme Style
									<select class="layout-style-option form-control input-small input-sm">
										<option value="square">Square corners</option>
										<option value="rounded" selected="selected">Rounded corners</option>
									</select>
								</li>
								<li>
									 Layout
									<select class="layout-option form-control input-small input-sm">
										<option value="fluid" selected="selected">Fluid</option>
										<option value="boxed">Boxed</option>
									</select>
								</li>
								<li>
									 Header
									<select class="page-header-option form-control input-small input-sm">
										<option value="fixed" selected="selected">Fixed</option>
										<option value="default">Default</option>
									</select>
								</li>
								<li>
									 Top Dropdowns
									<select class="page-header-top-dropdown-style-option form-control input-small input-sm">
										<option value="light">Light</option>
										<option value="dark" selected="selected">Dark</option>
									</select>
								</li>
								<li>
									 Sidebar Mode
									<select class="sidebar-option form-control input-small input-sm">
										<option value="fixed">Fixed</option>
										<option value="default" selected="selected">Default</option>
									</select>
								</li>
								<li>
									 Sidebar Menu
									<select class="sidebar-menu-option form-control input-small input-sm">
										<option value="accordion" selected="selected">Accordion</option>
										<option value="hover">Hover</option>
									</select>
								</li>
								<li>
									 Sidebar Position
									<select class="sidebar-pos-option form-control input-small input-sm">
										<option value="left" selected="selected">Left</option>
										<option value="right">Right</option>
									</select>
								</li>
								<li>
									 Footer
									<select class="page-footer-option form-control input-small input-sm">
										<option value="fixed">Fixed</option>
										<option value="default" selected="selected">Default</option>
									</select>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<!-- END THEME PANEL -->
		</div>
		<!-- END PAGE TOOLBAR -->
	</div>
	<!-- END PAGE HEAD -->
	<!-- BEGIN PAGE BREADCRUMB -->
	<ul class="page-breadcrumb breadcrumb hide">
		<li>
			<a href="javascript:;">Home</a><i class="fa fa-circle"></i>
		</li>
		<li class="active">
			 Category
		</li>
	</ul>
	<!-- END PAGE BREADCRUMB -->
	
	<div class="row margin-top-10">
		<div class="col-md-4">
			<div class="portlet light">
				<div class="portlet-body">
					@include('admin.layouts.flash-message')
					{{ Form::open(['route'=>'categories.store']) }}
						<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
							{{ Form::label('Name') }}
							{{ Form::text('name', old('name') ,['class'=>'form-control','placeholder'=>'Enter Name']) }}
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
						</div>
						<div class="form-group">
							{{ Form::label('Parent Category') }}
						  	{{ Form::select('parent_id',$options, 0,
						  	['class'=>'form-control select2','data-placeholder'=>'Select a State'])}}
                        </div>
						{{ Form::submit('Save',['class'=>'btn btn-primary'])}}
					{{ Form::close() }}
				</div>
				<!-- /.box-body -->
			</div>
			<!-- /.box -->
		</div>
		<div class="col-md-8">
			<div class="portlet light">
				<div class="portlet-body">
					<table id="example2" class="table table-bordered table-hover">
		                <thead>
			                <tr>
			                  	<th>S.N.</th>
			                  	<th>Name</th>
			                  	<th>Parent Id</th>
			                  	<th>Actions</th>
			                </tr>
		                </thead>
		                <tbody>
		                	@foreach($categories as $category)
			                <tr id="row-{{ $category->id }}">
			                  	<td>{{ ++$loop->index }}</td>
			                  	<td>{{ $category->name }}</td>
			                  	<td style="text-align: center;">{{ $category->parent_id}}</td>
			                  	<td style="text-align: center;">
			                  		<a href="{{ route('categories.edit', $category->id) }}">
										<i class="fa fa-edit" style="color:green; text-decoration: none;"></i>
									</a>
									<span style="padding-right: 2px;">|</span> 
								    <a class="delete-link" href="#" 
                                        data-url="{{ route('categories.destroy', $category->id) }}" 
                                        data-id="{{ $category->id }}" 
                                        data-token="{{ csrf_token() }}">
                                        <i class="fa fa-trash" style="color:red; text-decoration: none;"></i>
                                    </a>		               
			                  	</td>
			                </tr>
			                @endforeach   
		                </tbody>
		                <tfoot>
			                <tr>
			                  	<th>S.N.</th>
			                  	<th>Name</th>
			                  	<th>Parent Id</th>
			                  	<th>Actions</th>
			                </tr>
		                </tfoot>
	              	</table>
				</div>
			</div>
		</div>
	</div>

	<div class="modal modal-danger fade" id="modal-danger">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Are you sure?</h4>
                </div>
                <div class="modal-body">
                    <p>This record will be permanently deleted from the database.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button id="modal-yes-btn" type="button" class="btn btn-outline btn-danger" data-dismiss="modal">Yes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
@endsection

@section('js')
{{-- <script type="text/javascript" src="{{ asset('admin/assets/global/plugins/select2/select2.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ asset('admin/assets/global/plugins/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" 
{{-- src="{{ asset('admin/assets/global/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('admin/assets/global/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script> --}}
{{-- <script type="text/javascript" src="{{ asset('admin/assets/global/plugins/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script> --}}
<script type="text/javascript" src="{{ 
	asset('admin/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>

<script>
	// $('.select2').select2();
	$('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false,
      'lengthMenu'  : [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
    })

    $('.delete-link').click( function() {
        $('#modal-danger').modal('show');
        window.categoryId = $(this).data('id');
        window.url = $(this).data('url');
        window.token = $(this).data('token');
        window.categoryRowSelector = '#row-'+window.categoryId;
    });

    $('#modal-yes-btn').click( function() {
         $.ajax({
            type: 'post',
            url: window.url,
            data: {
                '_method': 'delete',
                '_token': window.token,
                'id': window.categoryId
            },
            success: function(data) {
                $(window.categoryRowSelector).fadeOut('slow');
            }
        });
    });
</script>
@endsection