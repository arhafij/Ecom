<?php

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// Customer Routes
Route::group(['namespace'=>'Customer'], function() {
	
	// carts
	Route::resource('carts', 'CartController');

	// orders
	Route::resource('orders', 'OrderController');

	Route::get('/cart', function () {
    	return view('customer.cart');
	});

	Route::get('/product', function () {
	    return view('customer.product');
	});

	Route::get('/category', function () {
	    return view('customer.category');
	});

	Route::get('/account', function () {
	    return view('customer.account');
	});
});

// Admin Routes
Route::group(['prefix'=>'admin','namespace'=>'Admin'], function() {
	
	Route::resource("products","ProductController");
	Route::resource("tags","TagController");
	Route::resource("categories","CategoryController");

	// Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login.show');
    Route::post('login', 'Auth\LoginController@login')->name('admin.login');
    Route::post('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register.show');
    Route::post('register', 'Auth\RegisterController@register')->name('admin.register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::get('/home', 'HomeController@index')->name('admin.home');

});


